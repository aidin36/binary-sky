use std::sync::{Mutex, Arc};
use engine;

pub mod pipe;

/// Starts all the required communicators.
pub fn start(mut engine: Arc<Mutex<engine::Engine>>) {
  // TODO: Decide which communicators to start base on the configs.
  pipe::start(Arc::clone(&engine));
}
