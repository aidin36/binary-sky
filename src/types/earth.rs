
pub enum Earth {
    Soil,
    Sand,
    Water,
    Rock,
}

impl Earth {
    pub fn value(&self) -> u8 {
        match *self {
            Earth::Soil => 0,
            Earth::Sand => 1,
            Earth::Water => 2,
            Earth::Rock => 3,
        }
    }

    pub fn from_char(value: char) -> Result<Earth, String> {
        match value {
            '0' => return Ok(Earth::Soil),
            '1' => return Ok(Earth::Sand),
            '2' => return Ok(Earth::Water),
            '3' => return Ok(Earth::Rock),
            x => return Err(format!("Unknown earth type: [{}]", x))
        }
    }
}
