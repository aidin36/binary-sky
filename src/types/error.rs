
use std::fmt;


pub enum Error {
    InvalidLocation(String),
    RobotNotFound(String),
    CannotPlant(String),
}

impl Error {
    pub fn code(&self) -> u8 {
        match *self {
            Error::InvalidLocation(_) => 1,
            Error::RobotNotFound(_) => 2,
            Error::CannotPlant(_) => 3,
        }
    }

    pub fn description(&self) -> &String {
        // TODO: There should be a cleaner way. We shouldn't have to list all enum here.
        match *self {
            Error::InvalidLocation(ref desc) => desc,
            Error::RobotNotFound(ref desc) => desc,
            Error::CannotPlant(ref desc) => desc,
        }
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{}] {}", self.code(), self.description())
    }
}