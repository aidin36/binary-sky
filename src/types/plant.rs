///
/// This module defines Plant struct which holds
/// information about an individual plant.
///

use std;
use std::time;
use config;


/// A light weight structure to hold information
/// about a specific plant.
pub struct PlantInfo {
    pub age: i16,
    pub water_level: i16,
}

/// Defines a plant.
pub struct Plant {
    age: i16,
    water_level: i16,
    last_update: time::Instant,
}

impl Plant {
    pub fn new() -> Plant {
        Plant {
            age: 0,
            water_level: config::get_plant_initial_water(),
            last_update: time::Instant::now()
        }
    }

    /// Gets all information about this plant.
    /// It returns None if plant is died.
    pub fn get_info(&mut self) -> Option<PlantInfo> {
        // is_alive will update the plant.
        if !self.is_alive() {
            return None;
        }

        // We don't call get methods, to avoid overhead
        // of updating. We already update the plant.
        let result = PlantInfo {
            age: self.age,
            water_level: self.water_level,
        };
        return Some(result);
    }

    /// Gets age of this plant.
    /// Note that it may become negative in some situations.
    pub fn get_age(&mut self) -> i16 {
        self.update();
        return self.age;
    }

    /// Gets water level of this plant.
    /// Note that it may become negative in some situations.
    pub fn get_water_level(&mut self) -> i16 {
        self.update();
        return self.water_level;
    }

    /// Adds water to the plant.
    /// It won't work if plant already died.
    pub fn add_water(&mut self, value: i16) {
        self.update();
        if !self.is_alive() {
            return;
        }

        // Preventing overflow.
        self.water_level = self.water_level.saturating_add(value);
    }

    /// Returns true if plant still alive.
    pub fn is_alive(&mut self) -> bool {
        self.update();
        if self.age >= config::get_plant_max_age() {
            return false;
        }
        if self.water_level <= 0 {
            return false;
        }
        return true;
    }

    /// Returns true if plant is matured (edible!)
    pub fn is_matured(&mut self) -> bool {
        self.update();
        if self.age >= config::get_plant_matured_age() {
            return true;
        }
        return false;
    }

    /// Updates the age and water level according to the time passed.
    fn update(&mut self) {

        // Calculating milliseconds passed from the previous update.
        let time_passed = self.last_update.elapsed();
        let real_millis_passed =
            ((time_passed.as_secs() as f64 + (time_passed.subsec_nanos() as f64 * 1e-9)) * 1000.0)
            as i64;

        let millis_passed: i16;
        if real_millis_passed > std::i16::MAX as i64 {
            millis_passed = std::i16::MAX;
        }
        else {
            millis_passed = real_millis_passed as i16;
        }

        // Updating values according to time passed and configurations.
        self.age = self.age.saturating_add(
            millis_passed * config::get_plant_age_per_millis());
        self.water_level = self.water_level.saturating_sub(
            millis_passed * config::get_plant_water_consum_per_millis());

        self.last_update = time::Instant::now();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::i16;
    use std::thread;
    use std::time;

    #[test]
    fn test_die_from_age() {
        let mut plant = Plant::new();
        // Adding a lot of water to prevent dying from lake of water.
        plant.add_water(i16::MAX);

        // Waiting for plant to die.
        // We add extra one millisecond for tolerance.
        // TODO: Mock configs or something, to increase the speed of the test.
        let sleep_time = config::get_plant_age_per_millis() * config::get_plant_max_age() + 1;
        thread::sleep(time::Duration::from_millis(sleep_time as u64));

        assert_eq!(plant.is_alive(), false);
    }

    #[test]
    fn test_die_from_water() {
        let mut plant = Plant::new();
        let sleep_time: i16 = plant.get_water_level() / config::get_plant_water_consum_per_millis();
        // We add extra one millisecond for tolerance.
        thread::sleep(time::Duration::from_millis(sleep_time as u64 + 1));

        assert_eq!(plant.is_alive(), false);
    }

    #[test]
    fn test_maturity() {
        let mut plant = Plant::new();
        // Adding a lot of water to prevent dying from lake of water.
        plant.add_water(i16::MAX);

        assert_eq!(plant.is_matured(), false);

        // Waiting for plant to be matured.
        // We add extra one millisecond for tolerance.
        // TODO: Mock configs or something, to increase the speed of the test.
        let sleep_time = config::get_plant_age_per_millis() * config::get_plant_matured_age() + 1;

        // Sleeping half the time. Plant shouldn't be matured.
        thread::sleep(time::Duration::from_millis((sleep_time / 2) as u64));
        assert_eq!(plant.is_matured(), false);

        // Sleeping the rest of the time.
        thread::sleep(time::Duration::from_millis((sleep_time / 2) as u64));
        assert_eq!(plant.is_matured(), true);
    }

    #[test]
    fn test_values_over_time() {
        let mut plant = Plant::new();
        let sleep_time: i16 = 5;

        plant.add_water(1000);
        let initial_water = plant.get_water_level();
        let initial_age = plant.get_age();

        thread::sleep(time::Duration::from_millis(sleep_time as u64));

        let current_water = plant.get_water_level();
        let current_age = plant.get_age();
        let expecting_water = initial_water - (sleep_time * config::get_plant_water_consum_per_millis());
        let expecting_age = initial_age + (sleep_time * config::get_plant_age_per_millis());

        assert_eq!(current_age, expecting_age);
        assert_eq!(current_water, expecting_water);
    }
}
