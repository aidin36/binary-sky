extern crate uuid;

use self::uuid::Uuid;

pub struct Robot {
    id: String,
}

impl Robot {
    pub fn new() -> Robot {
        Robot {
            id: Uuid::new_v4().simple().to_string(),
        }
    }

    pub fn get_id(&self) -> String {
        return self.id.clone();
    }
}
