
#[macro_use]
extern crate lazy_static;

use std::sync::{Mutex, Arc};

mod config;
mod types;
mod engine;
mod communicator;


fn main() {
    // Loading configurations.
    config::load_configs();

    // Starting the components.
    // TODO: Read map file path from configs.
    let map_file_path = String::from("settings/maps/small.map");
    let mut engine = Arc::new(Mutex::new(engine::Engine::new(&map_file_path)));

    let engine_thread_handler = engine::executer::start(Arc::clone(&engine));

    communicator::start(Arc::clone(&engine));

    // Waiting for the engine thread.
    engine_thread_handler.join().expect("Engine thread paniced!");
}
