use std::sync::Mutex;
use engine::task::Task;
use engine::Engine;

pub struct ActionTask {

}

impl Task for ActionTask {
    fn execute(&mut self, engine: &Mutex<Engine>) {

    }

    fn callback(&self) {

    }
}
