use std::sync::Mutex;
use std::collections::HashMap;
use engine::task::Task;
use engine::Engine;
use types;

/// Holds information about a single location on the map.
pub struct LocationInfo {
    earth_type: types::Earth,
    plant: types::Plant,
    robot_id: String,
}

pub struct InfoTask {
    robot_id: String,
    result: HashMap<(u16, u16), LocationInfo>,
    callback_func: fn(&HashMap<(u16, u16), LocationInfo>) -> (),
}

impl Task for InfoTask {
    fn execute(&mut self, engine: &Mutex<Engine>) {

    }

    fn callback(&self) {
        (self.callback_func)(&self.result);
    }
}

impl InfoTask {
    /// Sets a call back that will be called after task executed.
    /// The call back should return as soon as possible.
    pub fn set_callback(&mut self, callback: fn(&HashMap<(u16, u16), LocationInfo>) -> ()) {
        self.callback_func = callback;
    }
}
