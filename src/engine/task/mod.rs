
use std::sync::Mutex;
use engine::Engine;

mod info;
mod born;
mod action;

// Every task must implement this trait.
pub trait Task {
    /// Executes the task.
    fn execute(&mut self, engine: &Mutex<Engine>);

    // Calls the issuer of the task.
    fn callback(&self);
}
