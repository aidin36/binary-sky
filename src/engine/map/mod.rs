
use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use types;
use types::error::Error;

pub struct Map {
    width: u16,
    height: u16,
    earth_layer: HashMap<(u16, u16), types::Earth>,
    plants_layer: HashMap<(u16, u16), types::Plant>,
    robots_layer: HashMap<(u16, u16), types::Robot>,
    // A map of robot ID to robot location, for quicker access.
    robots_location: HashMap<String, (u16, u16)>,
}

impl Map {
    pub fn new(map_file: &String) -> Map {
        let f = File::open(map_file)
            .expect("Could not open specified map file!");
        let mut map_reader = BufReader::new(&f);

        let (width, height) = Map::read_map_size(&mut map_reader);

        let earth_layer = Map::read_map(&mut map_reader, width, height);

        Map {
            width: width,
            height: height,
            earth_layer: earth_layer,
            plants_layer: HashMap::new(),
            robots_layer: HashMap::new(),
            robots_location: HashMap::new(),
        }
    }

    /// Reads the size of the map from the first line of the file.
    /// It will panic if anything happen during reading of file.
    fn read_map_size(reader: &mut BufReader<&File>) -> (u16, u16) {
        let mut first_line = String::new();
        reader.read_line(&mut first_line)
            .expect("Could not read from map file!");

        let mut size_iter = first_line.trim().split(",");
        let width_str = size_iter.next().expect("Bad map file format: Could not read size.");
        let height_str = size_iter.next().expect("Bad map file format: Could not read size.");

        let width: u16 = width_str.parse::<u16>().expect("Bad map file: width is not a number");
        let height: u16 = height_str.parse::<u16>().expect("Bad map file: height is not a number");

        return (width, height)
    }

    /// Reads the map from file and creates a hashtable from it.
    /// It will panic if anything goes wrong while reading the file.
    fn read_map(reader: &mut BufReader<&File>, width: u16, height: u16)
            -> HashMap<(u16, u16), types::Earth> {

        let mut result: HashMap<(u16, u16), types::Earth> = HashMap::new();

        for y in 0..height {
            let mut line = String::new();
            reader.read_line(&mut line)
                .expect("Could not read from file. Maybe the size is incorrect?");
            let mut line_chars = line.chars();

            let line_len = line.len() as u16;
            // -1 is for the end-of-line character.
            if line_len <= 0 || (line_len - 1) != width {
                panic!(format!("Wrong line length. Line: {}", y));
            }

            for x in 0..width {
                let earth_type = match line_chars.next() {
                    Some(v) =>
                        types::Earth::from_char(v)
                            .expect(&*format!("Error in line: [{}, {}]", x, y)),
                    // We checked the length of line before. But just in case...
                    None => panic!(format!("Wrong line length. Line: {}", y)),
                };

                result.insert((x, y), earth_type);
            }
        }

        return result;
    }

    fn check_out_of_map(&self, location: &(u16, u16)) -> bool {
        if location.0 >= self.width || location.1 >= self.height {
            return true;
        }
        return false;
    }

    /// Checks if the specified location is blocked (if
    /// a robot can move into it). Returns true if location
    /// is blocked.
    pub fn is_blocked(&self, location: &(u16, u16)) -> bool {
        if self.check_out_of_map(&location) {
            // Out of scope.
            return true;
        }

        if self.earth_layer.get(&location).unwrap().value() == types::Earth::Rock.value() {
            // A rock blocked the way.
            return true;
        }

        let is_robot = match self.robots_layer.get(&location) {
            Some(_) => true,
            None => false
        };
        if is_robot {
            // Another robot blocked the way.
            return true;
        }

        return false;
    }

    /// Adds the specified robot to the map.
    /// If the location is blocked, it finds the nearest empty location to
    /// the specified one.
    /// It takes the ownership of the robot object.
    /// Returns the location robot added to.
    pub fn add_robot(&mut self, robot: types::Robot, location: &(u16, u16))
        -> Result<(u16, u16), Error> {

        // TODO: Should we check uniqueness of robot id here?
        if self.check_out_of_map(&location) {
            return Err(Error::InvalidLocation(String::from("Location is out of scope of the map.")));
        }

        // Checking if location is blocked.
        let free_location = *location;
        if self.is_blocked(&free_location) {
            // TODO: Find another location.
        }

        // Adding robot to the quick access map.
        self.robots_location.insert(robot.get_id(), free_location);

        // Adding robot to the location.
        self.robots_layer.insert(free_location, robot);

        return Ok(free_location);
    }

    // Gets a robot by its location.
    // Returns error if location is not valid.
    // Returns None if there's no robot in that location.
    pub fn get_robot(&self, location: &(u16, u16)) -> Result<Option<&types::Robot>, Error> {
        if self.check_out_of_map(&location) {
            return Err(Error::InvalidLocation(
                format!("location ({}, {}) is out of map.", location.0, location.1)));
        }

        match self.robots_layer.get(&location) {
            Some(robot) => return Ok(Some(&robot)),
            None => return Ok(None),
        };
    }

    // Gets a robot by its ID.
    // Returns error if no robot existed with this ID.
    pub fn get_robot_by_id(&self, robot_id: &String) -> Result<&types::Robot, Error> {
        let robot_location = match self.robots_location.get(robot_id) {
            Some(location) => location,
            None => return Err(Error::RobotNotFound(
                format!("No robot found with ID [{}]", robot_id))),
        };

        match self.robots_layer.get(&robot_location) {
            Some(robot) => return Ok(&robot),
            None => panic!(format!(
                "Map corrupted! robot [{}] existed, but is not on the map location [{}, {}]",
                robot_id, robot_location.0, robot_location.1)),
        };
    }

    // Gets earth type of a location.
    // Returns error if locations is not valid.
    pub fn get_earth_type(&self, location: &(u16, u16)) -> Result<u8, Error> {
        if self.check_out_of_map(&location) {
            return Err(Error::InvalidLocation(
                format!("location ({}, {}) is out of map.", location.0, location.1)));
        }

        return Ok(self.earth_layer.get(&location).unwrap().value());
    }

    pub fn add_plant(&mut self, location: &(u16, u16)) -> Result<(), Error> {
        if self.check_out_of_map(&location) {
            return Err(Error::InvalidLocation(
                format!("location ({}, {}) is out of map.", location.0, location.1)));
        }

        self.update_plant(&location);

        if self.plants_layer.get(location).is_some() {
            return Err(Error::CannotPlant(format!(
                "There's already a plant in location ({}, {})", location.0, location.1)));
        }

        let earth_type = self.earth_layer.get(&location).unwrap().value();
        if earth_type != types::Earth::Soil.value() {
            return Err(Error::CannotPlant(format!(
                "Earth type is not suitable for planting a plant. Location ({}, {}), type: {}",
                location.0, location.1, earth_type)));
        }

        // Everthing was OK! Adding the plant.
        self.plants_layer.insert(*location, types::Plant::new());

        return Ok(());
    }

    pub fn get_plant_info(&mut self, location: &(u16, u16))
            -> Result<Option<types::PlantInfo>, Error> {
        if self.check_out_of_map(&location) {
            return Err(Error::InvalidLocation(
                format!("location ({}, {}) is out of map.", location.0, location.1)));
        }

        self.update_plant(&location);

        match self.plants_layer.get_mut(location) {
            Some(plant) => return Ok(plant.get_info()),
            None => return Ok(None),
        };
    }

    /// Updates a plant information on the map. Returns reference to the plant.
    /// Returns None if not plant found in that location.
    fn update_plant(&mut self, location: &(u16, u16)) {
        // The scope is existed because both "get_mut" and "remove" will
        // borrow a mutable. We let go of the first borrow before calling "remove".
        {
            let plant = match self.plants_layer.get_mut(&location) {
                None => return (),
                Some(plant) => plant,
            };

            if plant.is_alive() {
                return ();
            }
        }

        // If we reach here, plant is died. It should be removed.
        self.plants_layer.remove(location);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::env::temp_dir;
    use std::io::Write;
    use std::thread;
    use std::time;
    use config;

    fn create_test_map_file() -> String {
        let mut map_file_path = temp_dir();
        map_file_path.push("binary_sky_test_18uye.map");

        // Creating a map file (Note that "create" will truncate the file if exists).
        // We don't expect any errors here, so we unwrap all results.

        let mut f = File::create(&map_file_path).unwrap();

        f.write_all(b"15,10\n").unwrap();
        f.write_all(b"321000000000000\n").unwrap();
        f.write_all(b"000000000000000\n").unwrap();
        f.write_all(b"000000000000000\n").unwrap();
        f.write_all(b"000000000000000\n").unwrap();
        f.write_all(b"000000000000000\n").unwrap();
        f.write_all(b"000000000000000\n").unwrap();
        f.write_all(b"000000000000333\n").unwrap();
        f.write_all(b"000000000000000\n").unwrap();
        f.write_all(b"000000000200000\n").unwrap();
        f.write_all(b"000100000000002\n").unwrap();
        f.write_all(b"Other lines of the map can be comments.\n").unwrap();

        return String::from(map_file_path.to_str().unwrap());
    }


    #[test]
    fn test_file_read() {
        let test_map_file = create_test_map_file();
        let map = Map::new(&test_map_file);

        // Checking size of map.
        assert_eq!(map.width, 15);
        assert_eq!(map.height, 10);

        // Checking type of some of the earth tiles.
        assert_eq!(map.earth_layer.get(&(0, 0)).unwrap().value(), types::Earth::Rock.value());
        assert_eq!(map.earth_layer.get(&(1, 0)).unwrap().value(), types::Earth::Water.value());
        assert_eq!(map.earth_layer.get(&(2, 0)).unwrap().value(), types::Earth::Sand.value());
        assert_eq!(map.earth_layer.get(&(3, 0)).unwrap().value(), types::Earth::Soil.value());
        assert_eq!(map.earth_layer.get(&(12, 6)).unwrap().value(), types::Earth::Rock.value());
        assert_eq!(map.earth_layer.get(&(9, 8)).unwrap().value(), types::Earth::Water.value());
        assert_eq!(map.earth_layer.get(&(3, 9)).unwrap().value(), types::Earth::Sand.value());
        assert_eq!(map.earth_layer.get(&(14, 9)).unwrap().value(), types::Earth::Water.value());
        assert_eq!(map.earth_layer.get(&(13, 9)).unwrap().value(), types::Earth::Soil.value());
    }

    #[test]
    #[should_panic(expected = "Bad map file: width is not a number")]
    fn test_file_bad_width() {
        let mut map_file_path = temp_dir();
        map_file_path.push("binary_sky_test_bad_file_15678.map");

        // Creating a map file (Note that "create" will truncate the file if exists).
        // We don't expect any errors here, so we unwrap all results.

        let mut f = File::create(&map_file_path).unwrap();

        f.write_all(b"1f,10\n").unwrap();

        let file_path = String::from(map_file_path.to_str().unwrap());

        let _map = Map::new(&file_path);
    }

    #[test]
    #[should_panic(expected = "Bad map file: height is not a number")]
    fn test_file_bad_height() {
        let mut map_file_path = temp_dir();
        map_file_path.push("binary_sky_test_bad_file_unhyt.map");

        // Creating a map file (Note that "create" will truncate the file if exists).
        // We don't expect any errors here, so we unwrap all results.

        let mut f = File::create(&map_file_path).unwrap();

        f.write_all(b"80,uy\n").unwrap();

        let file_path = String::from(map_file_path.to_str().unwrap());

        let _map = Map::new(&file_path);
    }

    #[test]
    #[should_panic(expected = "Bad map file format: Could not read size.")]
    fn test_file_missed_size() {
        let mut map_file_path = temp_dir();
        map_file_path.push("binary_sky_test_bad_file_15678.map");

        // Creating a map file (Note that "create" will truncate the file if exists).
        // We don't expect any errors here, so we unwrap all results.

        let mut f = File::create(&map_file_path).unwrap();

        f.write_all(b"1200004500\n").unwrap();

        let file_path = String::from(map_file_path.to_str().unwrap());

        let _map = Map::new(&file_path);
    }

    #[test]
    #[should_panic(expected = "Wrong line length. Line: 1")]
    fn test_wrong_map_short_width() {
        let mut map_file_path = temp_dir();
        map_file_path.push("binary_sky_test_bad_file_15678.map");

        // Creating a map file (Note that "create" will truncate the file if exists).
        // We don't expect any errors here, so we unwrap all results.

        let mut f = File::create(&map_file_path).unwrap();

        f.write_all(b"12,2\n").unwrap();
        f.write_all(b"000000000000\n").unwrap();
        f.write_all(b"00000\n").unwrap();

        let file_path = String::from(map_file_path.to_str().unwrap());

        let _map = Map::new(&file_path);
    }

    #[test]
    #[should_panic(expected = "Wrong line length. Line: 1")]
    fn test_wrong_map_long_width() {
        let mut map_file_path = temp_dir();
        map_file_path.push("binary_sky_test_bad_file_15678.map");

        // Creating a map file (Note that "create" will truncate the file if exists).
        // We don't expect any errors here, so we unwrap all results.

        let mut f = File::create(&map_file_path).unwrap();

        f.write_all(b"12,2\n").unwrap();
        f.write_all(b"000000000000\n").unwrap();
        f.write_all(b"000000000000000\n").unwrap();

        let file_path = String::from(map_file_path.to_str().unwrap());

        let _map = Map::new(&file_path);
    }

    #[test]
    #[should_panic(expected = "Wrong line length. Line: 2")]
    fn test_wrong_map_height() {
        let mut map_file_path = temp_dir();
        map_file_path.push("binary_sky_test_bad_file_15678.map");

        // Creating a map file (Note that "create" will truncate the file if exists).
        // We don't expect any errors here, so we unwrap all results.

        let mut f = File::create(&map_file_path).unwrap();

        f.write_all(b"5,3\n").unwrap();
        f.write_all(b"00000\n").unwrap();
        f.write_all(b"00000\n").unwrap();

        let file_path = String::from(map_file_path.to_str().unwrap());

        let _map = Map::new(&file_path);
    }

    #[test]
    #[should_panic(expected = "Error in line: [2, 1]: \"Unknown earth type: [7]\"")]
    fn test_bad_earth_type_1() {
        let mut map_file_path = temp_dir();
        map_file_path.push("binary_sky_test_bad_earth_4rt5.map");

        // Creating a map file (Note that "create" will truncate the file if exists).
        // We don't expect any errors here, so we unwrap all results.

        let mut f = File::create(&map_file_path).unwrap();

        f.write_all(b"5,2\n").unwrap();
        f.write_all(b"00000\n").unwrap();
        f.write_all(b"00700\n").unwrap();

        let file_path = String::from(map_file_path.to_str().unwrap());

        let _map = Map::new(&file_path);
    }

    #[test]
    #[should_panic(expected = "Error in line: [4, 0]: \"Unknown earth type: [f]\"")]
    fn test_bad_earth_type_2() {
        let mut map_file_path = temp_dir();
        map_file_path.push("binary_sky_test_bad_earth_9id12.map");

        // Creating a map file (Note that "create" will truncate the file if exists).
        // We don't expect any errors here, so we unwrap all results.

        let mut f = File::create(&map_file_path).unwrap();

        f.write_all(b"5,2\n").unwrap();
        f.write_all(b"0000f\n").unwrap();
        f.write_all(b"00000\n").unwrap();

        let file_path = String::from(map_file_path.to_str().unwrap());

        let _map = Map::new(&file_path);
    }

    #[test]
    fn test_blocking() {
        let test_map_file = create_test_map_file();
        let mut map = Map::new(&test_map_file);

        assert_eq!(map.is_blocked(&(0, 0)), true);
        assert_eq!(map.is_blocked(&(0, 2)), false);
        assert_eq!(map.is_blocked(&(0, 3)), false);
        assert_eq!(map.is_blocked(&(6, 12)), true);
        assert_eq!(map.is_blocked(&(6, 14)), true);
        assert_eq!(map.is_blocked(&(6, 0)), false);
        assert_eq!(map.is_blocked(&(14, 9)), false);

        // Test if there's a robot.
        map.add_robot(types::Robot::new(), &(7, 4));
        assert_eq!(map.is_blocked(&(7,4)), true);

        // Test out of scope.
        assert_eq!(map.is_blocked(&(15, 5)), true);
        assert_eq!(map.is_blocked(&(5, 10)), true);
        assert_eq!(map.is_blocked(&(15, 10)), true);
        assert_eq!(map.is_blocked(&(200, 100)), true);
    }

    #[test]
    fn test_plant_state() {
        let test_map_file = create_test_map_file();
        let mut map = Map::new(&test_map_file);

        let plant_location: (u16, u16) = (3, 5);

        map.add_plant(&plant_location);

        let plant_info = map.get_plant_info(&plant_location).unwrap();
        assert_eq!(plant_info.is_none(), false);

        // Wait until plant dies.
        let sleep_time: i16 = plant_info.unwrap().water_level / config::get_plant_water_consum_per_millis();
        // We add extra one millisecond for tolerance.
        thread::sleep(time::Duration::from_millis(sleep_time as u64 + 1));

        let none_plant_info = map.get_plant_info(&plant_location).unwrap();
        assert_eq!(none_plant_info.is_none(), true);
    }

    #[test]
    fn test_add_plant_errors() {
        let test_map_file = create_test_map_file();
        let mut map = Map::new(&test_map_file);

        // Invalid location.
        let bad_location: (u16, u16) = (15, 10);
        let bad_location_result = map.add_plant(&bad_location);
        assert!(bad_location_result.is_err());

        // Double plant.
        let plant_location: (u16, u16) = (3, 6);

        map.add_plant(&plant_location).unwrap();

        let result = map.add_plant(&plant_location);
        assert!(result.is_err());
    }
}
