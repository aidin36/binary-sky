
/// This module contains the heart of the engine.
/// It's a loop that executes tasks in the queue.

use engine::Engine;
use engine::task::Task;
use std::sync::{Mutex, Arc};
use std::thread;

pub fn start(mut engine: Arc<Mutex<Engine>>) -> thread::JoinHandle<()> {
    return thread::Builder::new()
                .name(String::from("EnginExecuterThread"))
                .spawn(move || {
                    run_forever(Arc::clone(&engine));
                })
                .unwrap();
}

fn run_forever(mut engine: Arc<Mutex<Engine>>) {
    loop {
        let mut task_option: Option<Box<Task + Send>>;

        {
            // We don't expect a panic on other threads. That's why we unwrapped.
            let mut engine = engine.lock().unwrap();
            task_option = engine.pop_task();
        }

        let mut task = match task_option {
            Some(t) => t,
            None => {
                // No task. Let the others do their job, and try again.
                thread::yield_now();
                continue;
            },
        };

        // Actually executing the task.
        task.execute(&engine);

        // Call back will send the result back to the caller.
        task.callback();
    }
}