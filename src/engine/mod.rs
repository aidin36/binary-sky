
use std::collections::VecDeque;
//use std::boxed::Box;
use engine::task::Task;

mod task;
mod map;
pub mod executer;

pub struct Engine {
    queue: VecDeque<Box<Task + Send>>,
    map: map::Map,
}

impl Engine {
    pub fn new(map_file: &String) -> Engine {
        Engine {
            queue: VecDeque::new(),
            map: map::Map::new(&map_file),
        }
    }

    pub fn push_task(&mut self, task: Box<Task + Send>) {
        self.queue.push_back(task);
    }

    pub fn pop_task(&mut self) -> Option<Box<Task + Send>> {
        return self.queue.pop_front();
    }
}
