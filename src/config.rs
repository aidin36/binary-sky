///
/// This module contains functions that returns values of
/// application configs.
///

extern crate lazy_static;

/// Holds configuration of the application.
struct Configs {
    plant_initial_water: i16,
    plant_water_consum_per_millis: i16,
    plant_age_per_millis: i16,
    plant_matured_age: i16,
    plant_max_age: i16,
}

lazy_static! {
    static ref CONFIGS: Configs = read_config_file();
}

/// Loads configs from the file. This should be called right
/// after program started.
pub fn load_configs() {
    lazy_static::initialize(&CONFIGS);
}

/// Private function to read config files.
fn read_config_file() -> Configs {
    // TODO: Actually read options from file.
    Configs {
        plant_initial_water: 50,
        plant_water_consum_per_millis: 5,
        plant_age_per_millis: 1,
        plant_matured_age: 400,
        plant_max_age: 3000,
    }
}

/// Gets water of the newly plant plant.
/// In other words, it determines how much time a robot have
/// to give the first water to the plant.
pub fn get_plant_initial_water() -> i16 {
    return CONFIGS.plant_initial_water;
}

/// Gets how much water level of a plant should be reduced in one milli second.
pub fn get_plant_water_consum_per_millis() -> i16 {
    return CONFIGS.plant_water_consum_per_millis;
}

/// Gets how old a plant will become every milli second.
pub fn get_plant_age_per_millis() -> i16 {
    return CONFIGS.plant_age_per_millis;
}

/// Gets at what age a plant will become matured.
pub fn get_plant_matured_age() -> i16 {
    return CONFIGS.plant_matured_age;
}

/// Gets the age that plant will die.
pub fn get_plant_max_age() -> i16 {
    return CONFIGS.plant_max_age;
}
